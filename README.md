# Purescript-test

A test project for the IOHK company using Haskell and Purescript.

## Setup

Created using Stack ("stack new purescript-test servant").


### Server

```bash
stack install
stack build
stack runghc app/Main.hs
```

### Frontend

Original local versions:

    npm     = 3.5.2
    node    = 4.2.6


```bash
npm install -g purescript pulp bower webpack
npm i

pulp browserify --to static/app.js
pulp build -O --to static/app.js
npm run package
```

After generation of PS code, please insert the show instances into 'PSGenerator':

import Data.Generic (class Generic, gShow)
import Data.Show

-----------------------------------------------------------------
instance showReportingOutput :: Show ReportingOutput where
  show = gShow

instance showTrackingOutput :: Show TrackingOutput where
  show = gShow
-----------------------------------------------------------------


