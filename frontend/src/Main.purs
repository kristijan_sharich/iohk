module Main where

import Prelude (bind, const, map, pure, show, ($), (<$>), (=<<))

import Control.Monad.Aff
import Control.Monad.Except.Trans (ExceptT, runExceptT)
import Control.Monad.Reader.Trans (ReaderT, runReaderT)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception (EXCEPTION)

import Data.Either (Either(..))
import Data.Generic (gShow)
import Data.Maybe (Maybe(..))
import Data.List (List(Nil, Cons))

import Data.Argonaut.Generic.Aeson (decodeJson, encodeJson)

import Servant.PureScript.Affjax (AjaxError)
import Servant.PureScript.Settings (SPSettings_(..))

import WebAPI (SPParams_(..), getReporting, getTracking)
import PSGenerator (TrackingOutput, ReportingOutput)

import Network.HTTP.Affjax (AJAX)

import Pux (App, Config, CoreEffects, EffModel, noEffects, renderToDOM, start)
import Pux.Html (Html, button, div, span, text)
import Pux.Html.Events (onClick)
import Pux.Html.Attributes (className)

import Signal.Channel (CHANNEL)


type AppEffects = (ajax :: AJAX)

type MySettings = SPSettings_ SPParams_
type APIEffect eff = ReaderT MySettings (ExceptT AjaxError (Aff (ajax :: AJAX, channel :: CHANNEL, err :: EXCEPTION  | eff)))

data Action = FetchTrackingOutput
            | FetchReportingOutput
            | ShowTrackingOutput TrackingOutput
            | ShowReportingOutput ReportingOutput
            | ReportError AjaxError
            | SubscriberLog String
            | Nop

type State = {
    result :: String
  , settings :: MySettings
  , lastError :: Maybe AjaxError
  , subscriberLog :: List String
  }

runEffectActions :: State -> Array (APIEffect () Action) -> EffModel State Action (ajax :: AJAX)
runEffectActions state effects = { state : state, effects : map (runEffect state.settings) effects }

config :: forall eff. Eff (channel :: CHANNEL, err :: EXCEPTION, ajax :: AJAX | eff) (Config State Action AppEffects)
config = do
    let settings = SPSettings_ {
                    encodeJson : encodeJson
                  , decodeJson : decodeJson
                  , toURLPiece : gShow
                  , params : SPParams_ {
                      baseURL : "http://localhost:5000/"
                    }
                  }
    pure 
        { initialState: { result : "", settings : settings, lastError : Nothing, subscriberLog : Nil }
        , update: update
        , view: view
        , inputs: []
        }

main :: Eff (CoreEffects AppEffects) (App State Action)
main = do
    app <- start =<< config
    renderToDOM "#app" app.html
    pure app


runEffect :: MySettings -> APIEffect () Action -> Aff (channel :: CHANNEL, err :: EXCEPTION, ajax :: AJAX) Action
runEffect settings m = do
    er <- runExceptT $ runReaderT m settings
    case er of
      Left err -> pure $ ReportError err
      Right v -> pure v

--errorToString :: AjaxError -> String
--errorToString = unsafeToString

view :: State -> Html Action
view state =
  div [ className "container" ]
    [ div
        [ className "lead"]
        [ button [ className "btn btn-lg btn-warning", onClick (const FetchTrackingOutput) ] [ text "Tracking" ]
        , button [ className "btn btn-lg btn-danger", onClick (const FetchReportingOutput) ] [ text "Reporting" ]
        , div [className "panel panel-success"] 
          [ div [className "panel-heading"] [text "Server output"] 
          , div [className "panel-body"] [text (show state.result)] 
          ]
        , div [className "panel panel-danger"] 
          [ div [className "panel-heading"] [text "Server errors"] 
          , div [className "panel-body"] [text (show state.lastError)] 
          ]  
        ]
    ]

update :: Action -> State -> EffModel State Action (ajax :: AJAX)
update FetchTrackingOutput state = runEffectActions state [ShowTrackingOutput <$> getTracking]
update FetchReportingOutput state = runEffectActions state [ShowReportingOutput <$> getReporting]

update (ShowTrackingOutput val)  state = { state : state { result = show val }, effects : []}
update (ShowReportingOutput val) state = { state : state { result = show val }, effects : []}

update (ReportError err ) state = { state : state { lastError = Just err}, effects : []}
update (SubscriberLog msg) state = { state : state { subscriberLog = Cons msg state.subscriberLog}, effects : []}
update Nop state = noEffects state