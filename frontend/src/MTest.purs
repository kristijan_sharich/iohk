module MTest where

import Prelude

import Data.StrMap (StrMap)

import Data.List
import Data.Types (NominalDiffTime)

import Data.Generic
import Data.Foreign
import Data.Foreign.Generic
import Data.Foreign.Class

import Data.Show
import Data.Eq
import Data.Ord
import Data.Either
import Data.Generic.Rep as G

data TrackingOutput = TrackingOutput { output :: List (StrMap NominalDiffTime) }
derive instance genericTrackingOutput :: G.Generic TrackingOutput _

data ReportingOutput = ReportingOutput { output :: StrMap NominalDiffTime }
derive instance genericReportingOutput :: G.Generic ReportingOutput _
