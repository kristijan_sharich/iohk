{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE DeriveGeneric         #-}

module WebAPI
    ( startApp
    , api
    , TrackingOutput
    , ReportingOutput
    ) where

import Data.Aeson
import Data.Aeson.TH
import Network.Wai
import Network.Wai.Handler.Warp
import GHC.Generics (Generic)
import Servant
import Tracking (TrOutput, ReOutput, ReOutputNew, trackingExample, reportExampleNew)
import Control.Monad.IO.Class


data TrackingOutput  = TrackingOutput [[TrOutput]] deriving (Generic, Show, Eq)
data ReportingOutput = ReportingOutput ReOutputNew deriving (Generic, Show, Eq)

instance FromJSON TrackingOutput
instance FromJSON ReportingOutput

instance ToJSON TrackingOutput
instance ToJSON ReportingOutput

type API = "tracking"   :> Get '[JSON] TrackingOutput
      :<|> "reporting"  :> Get '[JSON] ReportingOutput

type FullAPI = API :<|> Raw

startApp :: IO ()
startApp = run 5000 app

app :: Application
app = serve apiFull fullServer 

api :: Proxy API
api = Proxy

apiFull :: Proxy FullAPI
apiFull = Proxy

files :: Application
files = serveDirectory "frontend/static/"

server :: Server API
server = return (TrackingOutput trackingExample)
    :<|> return (ReportingOutput reportExampleNew)

fullServer :: Server FullAPI
fullServer = server :<|> files
    

