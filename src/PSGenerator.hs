{-# LANGUAGE AutoDeriveTypeable    #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}

module PSGenerator where

import           Control.Applicative
import           Control.Lens
import           Data.Typeable
import           Data.Aeson
import           Data.Monoid
import           Data.Proxy
import qualified Data.Set                           as Set
import           Data.Text                          (Text)
import qualified Data.Text                          as T
import qualified Data.Text.Encoding                 as T
import qualified Data.Text.IO                       as T
import           Language.PureScript.Bridge
import           Language.PureScript.Bridge.PSTypes
import           Language.PureScript.Bridge.TypeInfo (PSType, TypeInfo (..))
import           Servant.API
import           Servant.PureScript
import           Servant.Subscriber.Subscribable
import Control.Monad.Reader

import           Tracking
import           WebAPI (api, TrackingOutput, ReportingOutput)

fixTypesModule :: BridgePart
fixTypesModule = do
  typeModule ^== "WebAPI"
  t <- view haskType
  TypeInfo (_typePackage t) "PSGenerator" (_typeName t) <$> psTypeParameters

myBridge :: BridgePart
myBridge = defaultBridge <|> posixTimeBridge <|> fixTypesModule 
-- <|> mapBridge


data MyBridge

myBridgeProxy :: Proxy MyBridge
myBridgeProxy = Proxy

instance HasBridge MyBridge where
  languageBridge _ = buildBridge myBridge


posixTimeBridge :: BridgePart
posixTimeBridge = typeName ^== "NominalDiffTime" >> pure psPosixTime

mapBridge :: BridgePart
mapBridge = typeName ^== "Map" >> psMap

psPosixTime :: PSType
psPosixTime = TypeInfo "" "Data.Types" "NominalDiffTime" []

-- TODO: headOption, tailOption!
psMap :: MonadReader BridgeData m => m PSType
psMap = do
    t1 <- head <$> psTypeParameters
    t2 <- head <$> tail <$> psTypeParameters
    pure $ TypeInfo "purescript-prim" "Prim" "Array"
        [ TypeInfo "purescript-tuples" "Data.Tuple" "Tuple"
             [ t1
             , t2
             ]
        ]

myTypes :: [SumType 'Haskell]
myTypes = [
            mkSumType (Proxy :: Proxy TrackingOutput)
          , mkSumType (Proxy :: Proxy ReportingOutput)
          ]

mySettings :: Settings
mySettings = (defaultSettings & apiModuleName .~ "WebAPI") {
  _generateSubscriberAPI = False
  }

main :: IO ()
main = do
  let frontEndRoot = "frontend/src"
  writeAPIModuleWithSettings mySettings frontEndRoot myBridgeProxy api
  writePSTypes frontEndRoot (buildBridge myBridge) myTypes




